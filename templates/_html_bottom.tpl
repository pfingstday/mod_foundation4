<script>
    document.write('<script src=' +
    ('__proto__' in {} ? 'lib/foundation4/js/vendor/zepto' : 'lib/foundation4/js/vendor/jquery') +
    '.js><\/script>')
</script>
{% lib
    "foundation4/js/foundation/foundation.js"
    "foundation4/js/foundation/foundation.alerts.js"
    "foundation4/js/foundation/foundation.clearing.js"
    "foundation4/js/foundation/foundation.cookie.js"
    "foundation4/js/foundation/foundation.dropdown.js"
    "foundation4/js/foundation/foundation.forms.js"
    "foundation4/js/foundation/foundation.joyride.js"
    "foundation4/js/foundation/foundation.magellan.js"
    "foundation4/js/foundation/foundation.orbit.js"
    "foundation4/js/foundation/foundation.placeholder.js"
    "foundation4/js/foundation/foundation.reveal.js"
    "foundation4/js/foundation/foundation.section.js"
    "foundation4/js/foundation/foundation.tooltips.js"
    "foundation4/js/foundation/foundation.topbar.js"
    "foundation4/js/foundation/foundation.interchange.js"
%}
<script>
    $(document).foundation();
</script>