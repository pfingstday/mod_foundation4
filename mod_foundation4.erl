-module(mod_foundation4).
-author("Steffen Hanikel <steffen.hanikel@gmail.com>").

-mod_title("Foundation 4 Framework").
-mod_description("The most advanced responsive front-end framework in the world.").
-mod_prio(900).
-mod_depends([base, admin]).
-mod_provides([foundation4]).

%% interface functions
-export([
  observe_admin_menu/3
]).

-include_lib("zotonic.hrl").
-include_lib("modules/mod_admin/include/admin_menu.hrl").

observe_admin_menu(admin_menu, Acc, Context) ->
  [
    #menu_item{id=admin_foundation4,
    parent=admin_modules,
    label=?__("Foundation 4", Context),
    url={admin_foundation4},
    visiblecheck={acl, use, ?MODULE}}

    |Acc].
