-module(controller_admin_foundation4).
-author("Steffen Hanikel <steffen.hanikel@gmail.com>").

-export([
  is_authorized/2,
  event/2
]).

-include_lib("controller_html_helper.hrl").

is_authorized(ReqData, Context) ->
  z_acl:wm_is_authorized(use, mod_foundation4, ReqData, Context).


html(Context) ->
  Vars = [
    {page_admin_foundation4, true}
  ],
  Html = z_template:render("admin_foundation4.tpl", Vars, Context),
  z_context:output(Html, Context).


event(#submit{message=admin_foundation4_customize}, Context) ->
  case z_acl:is_allowed(use, mod_foundation4, Context) of
    true ->
      Vars = proplists:get_value("foundation4-vars", z_context:get_q_all(Context)),
      m_config:set_value("foundation4","vars", Vars, Context),
      m_config:set_prop("foundation4","vars", no_config_edit, true, Context),
      case z_module_indexer:find(lib, "foundation4/scss/template.scss", Context) of
        {ok, #module_index{filepath=FilePath}} ->
          SitePath = site_path(z_context:site(Context)),
          OutputPath = filename:join(SitePath, "lib/foundation4/css/foundation.min.css"),
          VarsPath = filename:join(SitePath, "lib/foundation4/vars.scss"),
          file:write_file(VarsPath, Vars),
          filelib:ensure_dir(OutputPath),
          Result = compile(FilePath, OutputPath, filename:join(SitePath, "lib/foundation4/")),
          case Result == [] of
            true ->
              z_render:growl("Finished!", Context);
            false ->
              z_render:growl("Error while compiling: " ++ Result, Context)
          end;
        {error, enoent} ->
          z_render:growl("Could not find the path of the scss template.", Context)
      end;
    false ->
      z_render:growl("You don't have permission to customize the Foundation CSS.", Context)
  end.

compile(InPath, OutPath, IncludePath) ->
  Command = lists:flatten(["sass --no-cache --style compressed -I '", IncludePath, "' '", InPath, "' '", OutPath, "'"]),
  os:cmd(Command).

site_path(Site) ->
  filename:join([z_utils:lib_dir(priv), "sites", Site]).
