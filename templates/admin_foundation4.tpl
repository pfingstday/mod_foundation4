{% extends "admin_base.tpl" %}

{% block title %}{_ Foundation 4 _}{% endblock %}

{% block content %}

<div class="admin-header">

    <h2>{_ Foundation 4 _}</h2>

    <p>{_ Here you can customize the Foundation 4 template. _}</p>
</div>

{% wire id="admin-foundation4-settings" type="submit" postback="admin_foundation4_customize" %}
<form id="admin-foundation4-settings" method="POST" action="postback">

    <div class="widget">

        <h3 class="widget-header">{_ General Foundation 4 Options _}</h3>
        <div class="widget-content">

            <div class="control-group">
                <div class="controls">
                    <textarea id="foundation4-vars" name="foundation4-vars" rows="15" class="span8">{{ m.config.foundation4.vars.value|escape }}</textarea>
                </div>
            </div>

<div class="control-group">
        <button class="btn btn-primary" type="submit">{_ Update CSS _}</button>
        </div>
        </div>

    </div>

</form>

{% endblock %}
